﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Clase principal encargada del control del juego y de guardar
 * informacion entre escenas, como la lista de personajes, jugadores 
 * o el volumen.
 * Tambien contiene la inicializacion de los personajes y hace
 * de mediador entre cualquier clase y PlayManager durante la partida,
 * ya que GameManager es la unica referencia que tienen todas las clases.
 * 
 * Se inicia en la primera escena que se ejecuta (TitleLogoScene) para
 * no tener que inicializarse de nuevo y se guarda como GameObject no dstruible,
 * ya que solo puede haber uno y esta escena solo se carga una vez.
 */
public class GameManager : MonoBehaviour {

    public int NumOfAngles, GameTimeInSeconds, BrownNumArrows, DiePoints;
    public float MaxWheel, MaxForce, GreenSpecialDistance;
    public Color MinColorArrow, MaxColorArrow;
    public float JumpForce;
    public LayerMask CollisionMask, TerrainMask;
    //public GameObject SelectedProjectilePrefab;
    public GameObject[] WeaponsPrefabs;
    public Dictionary<string, int> SpeedDict;

    public List<CharacterInfo> players, characters;

    public PlayManager playManager;

    public float EffectsVolume, MusicVolume;


    private void Awake() {
        EffectsVolume = PlayerPrefs.GetFloat("effects", 0.8f);
        MusicVolume = PlayerPrefs.GetFloat("music", 0.35f);

        SpeedDict = new Dictionary<string, int> {
            ["Slow"] = 150,
            ["Medium"] = 175,
            ["Fast"] = 200
        };
        
        DontDestroyOnLoad(gameObject);
        InitChars();
    }

    public void UpdateVolume(float effects, float music) {
        EffectsVolume = effects;
        PlayerPrefs.SetFloat("effects", effects);
        MusicVolume = music;
        PlayerPrefs.SetFloat("music", music);
    }

    /**
     * Metodo llamado desde cualquier clase que necesite un sonido, ya sea
     * efecto o cancion, durante la partida.
     * Este a su vez llama al metodo de PlayManager encargado de hacer sonar
     * dicho sonido.
     */
    public void PlaySoundEffect(PlayManager.Effects e) {
        playManager.PlaySoundEffect(e);
    }

    
    public CharacterInfo GetCharacter(int c) {
        return characters[c];
    }

    /**
     * Indica a PlayerManager el arma seleccionada por la IA.
     */
    public void SelectWeapon(int w) {
        playManager.SelectWeapon(w);
    }
    public int GetSelectedWeapon() {
        return playManager.SelectedWeapon;
    }


    /**
     * Metodo ejecutado desde MenuScene al pulsar el boton "Play!".
     * Se encarga de actualizar la informacion sobre cuales 
     * personajes son jugadores y finalmente inicia la escena de  
     * la partida.
     */
    public void StartGame(List<int[]> chars) {
        for (int i = 0; i < characters.Count; i++) {
            CharacterInfo c = characters[i];
            c.isPlayer = chars[i][0] != -1;
            if (c.isPlayer)
                c.playerNum = chars[i][1];
        }
        SceneManager.LoadScene("GameScene");
    }

    /**
     * Llamado desde CharacterController para obtener los prefabs 
     * de sus armas.
     */
    public GameObject[] GetWeaponsPrefabs(CharacterController cc) {
        foreach (CharacterInfo ci in characters)
            if (ci.controller.Equals(cc))
                return ci.weaponsPrefabs;
        return null;
    }



    public Transform GetSafeRespawnPoint() {
        return playManager.GetSafeRespawnPoint();
    }


    /**
     * Metodo llamado desde un personaje para obtener uno aleatorio
     * de entre los otros 3. 
     * Para evitar obtener el mismo que lo llama primero los pone 
     * en una lista los otros 3, y luego seleccionada de ahi
     * uno aleatoriamente.
     */
    public Transform GetRandomCharacter(CharacterController cc) {
        List<CharacterInfo> aux = new List<CharacterInfo>(characters);
        foreach (CharacterInfo ci in aux)
            if (ci.controller.Equals(cc)) {
                aux.Remove(ci);
                break;
            }
        return aux[Random.Range(0, aux.Count)].controller.transform;
    }

    /**
     * Metodo llamado desde Projectile a traves de CharacterController
     * para saber la posicion de los demas personajes (arma especial
     * del personaje Green).
     */
    public Transform[] GetOthersPositions(CharacterController cc) {
        List<Transform> l = new List<Transform>();
        foreach (CharacterInfo ci in characters) {
            if (ci.controller.Equals(cc))
                continue;
            l.Add(ci.controller.transform);
        }
        return l.ToArray();
    }

    /**
     * Metodo llamado desde CharacterController para obtener las 
     * posiciones X de los demas personajes y añadir una Y concreta,
     * en la parte superior de la pantalla (arma especial del 
     * personaje Pink).
     */
    public Vector3[] GetOthersUpOutsidePositions(CharacterController cc) {
        List<Vector3> positions = new List<Vector3>();
        foreach (CharacterInfo ci in characters) {
            if (ci.controller.Equals(cc))
                continue;
            Vector3 p = ci.controller.transform.position;
            p.y = playManager.UpOutsidePosition.position.y;
            positions.Add(p);
        }
        return positions.ToArray();
    }


    public void SetSpecialWepUnits(int u) {
        playManager.SetSpecialWepUnits(u);
    }

    public void SetShootingForceText(int f) {
        playManager.SetShootingForceText(f);
    }


    public void GameOver() {
        playManager.GameOver();
    }



    /**
     * Metodo que inicializa los 4 personajes con sus atributos y
     * los prefabs de sus armas.
     */
    private void InitChars() {
        GameObject[] ws = WeaponsPrefabs;
        CharacterInfo c0 = new CharacterInfo {
            healthPoints = 1000,
            speed = "Medium",
            damage = 175,
            explosionRange = 1.1f,
            weaponsPrefabs = new GameObject[] { ws[0], ws[1] },
            specialWepText = "Antigravity projectile"
        };
        CharacterInfo c1 = new CharacterInfo {
            healthPoints = 1100,
            speed = "Slow",
            damage = 225,
            explosionRange = 0.8f,
            weaponsPrefabs = new GameObject[] { ws[2], ws[3] },
            specialWepText = "Little projectiles from sky to the enemies"
        };
        CharacterInfo c2 = new CharacterInfo {
            healthPoints = 1000,
            speed = "Medium",
            damage = 200,
            explosionRange = 0.7f,
            weaponsPrefabs = new GameObject[] { ws[4], ws[5] },
            specialWepText = "Rain of projectiles"
        };
        CharacterInfo c3 = new CharacterInfo {
            healthPoints = 900,
            speed = "Fast",
            damage = 175,
            explosionRange = 1f,
            weaponsPrefabs = new GameObject[] { ws[6], ws[7] },
            specialWepText = "Magnetized bomb which gets attached to a nearby enemy"
        };
        characters = new List<CharacterInfo>(4) {c0, c1, c2, c3};
    }

    public int GetSpeedAsNum(string s) {
        return SpeedDict[s];
    }

    
    /**
     * Clase pequeña con toda la informacion relativa a cada personaje,
     * incluyendo su CharacterController.
     */
    public class CharacterInfo {
        public int healthPoints;
        public string speed;
        public int damage;
        public float explosionRange;
        public GameObject[] weaponsPrefabs;
        public string specialWepText;
        public int playerNum;
        public bool isPlayer;
        public CharacterController controller;
    }

}
