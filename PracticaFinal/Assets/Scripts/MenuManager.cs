﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase encargada de controlar el funcionamiento de la escena MenuScene.
 */
public class MenuManager : MonoBehaviour {

    [SerializeField]
    private Text HealthText, SpeedText, DamageText, ExplosionRangeText, SpecialWepText;
    [SerializeField]
    private Image bigCharImage, questionMarkImage, BaseWepImage, SpecialWepImage;
    [SerializeField]
    private Image[] charsImages, playersImages;
    [SerializeField]
    private Text[] playersTexts;
    [SerializeField]
    private Button[] playersButtons, xButtons, charsButtons;
    [SerializeField]
    private Button PlayButton, OptionsButton, CreditsButton, HowToPlayButton;
    [SerializeField]
    private Text InfoText, HowToPlayText;

    private GameManager manager;
    private int playerSelected;
    private Dictionary<int, int> alreadySelectedChars;


    private void Awake() {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        UpdateVolume();
        MenuSongSound.Play();
        playerSelected = -1;
        PlayButton.interactable = false;
        alreadySelectedChars = new Dictionary<int, int>();
        alreadySelectedChars[0] = alreadySelectedChars[1] =
            alreadySelectedChars[2] = alreadySelectedChars[3] = -1;
        ShowMenuCharacter(0);
        StartCoroutine(HowToPlayTextEffect());
    }

    /**
     * Corutina para hacer un simple efecto de color (blanco <-> amarillo)
     * sobre el texto del boton HowToPlay para resaltarlo y que el jugador
     * lo mire antes de jugar.
     */
    private IEnumerator HowToPlayTextEffect() {
        while (Application.isPlaying) {
            float progress = 0, time = 0.75f;
            while (progress <= 1) {
                progress += Time.deltaTime / time;
                HowToPlayText.color = Color.Lerp(Color.white, Color.yellow, progress);
                yield return null;
            }
            while (progress >= 0) {
                progress -= Time.deltaTime / time;
                HowToPlayText.color = Color.Lerp(Color.white, Color.yellow, progress);
                yield return null;
            }
        }
    }


    /**
     * Metodo llamado cuando el jugador selecciona uno de los botones
     * azules de la columna de la izquierda.
     */
    public void SelectPlayer(int i) {
        AddPlayerSound.Play();
        playerSelected = i;
    }

    /**
     * Metodo llamado cuando el jugador pulsa sobre uno de los botones 
     * X al lado de los botones azules para quitar ese jugador.
     * Comprueba que solo si hay mas de un jugador se puede pulsar uno
     * de ellos, ya que debe haber un jugador minimo.
     * Al quitar un jugador tambien actualiza la columna de la derecha
     * para habilitar de nuevo el personaje que tenia ese jugador.
     */
    public void RemovePlayer(int i) {
        RemovePlayerSound.Play();
        int c = alreadySelectedChars[i];
        if (c != -1) {
            charsButtons[c].interactable = true;
            alreadySelectedChars[i] = -1;
        }
        xButtons[i].interactable = false;
        playersButtons[i].interactable = true;
        if (i < 3)
            playersButtons[i + 1].interactable = false;
        playersImages[i].sprite = questionMarkImage.sprite;
        playersTexts[i].text = "AI";
        if (i > 1) {
            xButtons[i - 1].interactable = true;
        }
    }


    /**
     * Metodo llamado cuando el jugador pasa el mouse por encima
     * de uno de los botones de la columna de la derecha para mostrar
     * en la parte central el personaje de dicho boton con su
     * informacion.
     */
    public void ShowMenuCharacter(int i) {
        GameManager.CharacterInfo c = manager.GetCharacter(i);
        HealthText.text = c.healthPoints + "";
        SpeedText.text = c.speed;
        DamageText.text = c.damage + "";
        ExplosionRangeText.text = c.explosionRange + "m";
        SpecialWepText.text = c.specialWepText;
        bigCharImage.sprite = charsImages[i].sprite;
        BaseWepImage.sprite = c.weaponsPrefabs[0].GetComponent<SpriteRenderer>().sprite;
        SpecialWepImage.sprite = c.weaponsPrefabs[1].GetComponent<SpriteRenderer>().sprite;
    }

    /**
     * Metodo llamado cuando el jugador pulsa sobre uno de los botones
     * de la columna de la derecha.
     * Al pulsar en uno de ellos se comprueba si se habia seleccionado
     * antes uno de los botones de la izquierda, y si es asi entonces
     * se asigna el personaje seleccionado a ese jugador y queda 
     * registrado un nuevo jugador con ese personaje, deshabilitandolo
     * para que otros jugadores no lo puedan selccionar.
     */
    public void SelectMenuCharacter(int i) {
        SelectCharSound.Play();
        if (playerSelected != -1) {
            charsButtons[i].interactable = false;
            int c = alreadySelectedChars[playerSelected];
            if (c != -1)
                charsButtons[c].interactable = true;
            alreadySelectedChars[playerSelected] = i;
            playersImages[playerSelected].sprite = charsImages[i].sprite;
            if (playerSelected > 0) {
                xButtons[playerSelected].interactable = true;
                if (playerSelected > 1)
                    xButtons[playerSelected - 1].interactable = false;
            }
            playersTexts[playerSelected].text = "Player " + (playerSelected + 1);
            int next = playerSelected + 1;
            if (next < 4) {
                playersButtons[next].interactable = true;
            }
            PlayButton.interactable = true;
            playerSelected = -1;
        }
    }


    /**
     * Boton para ir a la escena HowToPlay
     */
    public void HowToPlay() {
        AcceptSound.Play();
        SceneManager.LoadScene("HowToPlayScene");
    }
    /**
     * Boton para ir a la escena Options
     */
    public void Options() {
        AcceptSound.Play();
        SceneManager.LoadScene("OptionsScene");
    }
    /**
     * Boton para ir a la escena Credits
     */
    public void Credits() {
        AcceptSound.Play();
        SceneManager.LoadScene("CreditsScene");
    }

    /**
     * Boton para ir a la escena GameScene y empezar la partida.
     * Para ello primero organiza la informacion (parejas de
     * jugador-personaje seleccionadas) y se la pasa al 
     * GameManager, que desde ahi ya guarda la informacion de
     * los personajes e inicia la partida.
     */
    public void Play() {
        AcceptSound.Play();
        DisableButtons();
        chars = new List<int[]>(4) {
            new int[] { -1, -1 },    //{char, playerNum}
            new int[] { -1, -1 },
            new int[] { -1, -1 },
            new int[] { -1, -1 }
        };
        foreach (KeyValuePair<int, int> pair in alreadySelectedChars) {
            int v = pair.Value;
            if (v != -1) {
                chars[v][0] = v;
                chars[v][1] = pair.Key;
            }
        }
        foreach (KeyValuePair<int, int> pair in alreadySelectedChars) {
            if (pair.Value == -1) {
                bool found = false;
                for (int j = 0; j < charsButtons.Length && !found; j++) {
                    Button b = charsButtons[j];
                    if (b.IsInteractable()) {
                        playersImages[pair.Key].sprite = charsImages[j].sprite;
                        b.interactable = false;
                        found = true;
                    }
                }
            }
        }
        InfoText.text = "Fighters getting ready...";
        Invoke("StartGame", 2);
    }

    List<int[]> chars;
    private void StartGame() {
        manager.StartGame(chars);
    }



    /**
     * Metodo para evitar que, una vez iniciandose la partida,
     * el usuario pueda pulsar cualquier otro boton.
     */
    private void DisableButtons() {
        foreach (Button b in playersButtons)
            b.interactable = false;
        foreach (Button b in xButtons)
            b.interactable = false;
        foreach (Button b in charsButtons)
            b.interactable = false;
        PlayButton.interactable = OptionsButton.interactable = 
            CreditsButton.interactable = HowToPlayButton.interactable = false;
    }





    ////// SOUND

    [SerializeField]
    private AudioSource MenuSongSound, 
        AcceptSound, AddPlayerSound, RemovePlayerSound, SelectCharSound;

    /**
     * Actualiza el volumen al entrar en la escena.
     */
    private void UpdateVolume() {
        MenuSongSound.volume = manager.MusicVolume;
        AcceptSound.volume = AddPlayerSound.volume =
            RemovePlayerSound.volume = SelectCharSound.volume = manager.EffectsVolume;
    }
}
