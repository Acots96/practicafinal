﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Projectile;

/**
 * Clase que controla el comportamiento de la destruccion de una parte del terreno,
 * tanto para modificar la textura como el collider.
 *
 * Cada terreno esta formado por un objeto principal que contiene la textura y tantos
 * hijos (PolygonCollider2D) como trozos separados de terreno tenga dicha textura.
 * Los detalles de la implementacion estan en las cabeceras de los metodos y como
 * comentarios dentro de los mismos sobre cada fragmento de codigo.
 * 
 * Como mejora se podrian analizar mas a fondo todos los casos posibles de destruccion
 * de terreno, ya que cubre la gran mayoria de ellos pero alguna vez no tiene 
 * exactamente el comportamiento esperado, dando lugar a algun error, como alguna vez
 * ocurre con los disparos muy seguidos en una sola zona (personaje Brown), por lo que
 * finalmente he decidido que esa lluvia de proyectiles no afecte al terreno.
 */
public class TerrainController : MonoBehaviour {

    private Transform tr;
    private SpriteRenderer sr;

    private Texture2D texture;
    private List<PolygonCollider2D> polygons;


    private void Awake() {
        tr = transform;
        sr = GetComponent<SpriteRenderer>();
        Texture2D originalTex = sr.sprite.texture;
        texture = new Texture2D(originalTex.width, originalTex.height, TextureFormat.RGBA32, false);
        Graphics.CopyTexture(originalTex, texture);

        polygons = new List<PolygonCollider2D>(GetComponentsInChildren<PolygonCollider2D>());
    }


    /**
     * Aplica la explosion para modificar el collider y la textura.
     * 
     * Funciona bastante bien pero algunas veces falla por algunos
     * casos concretos que no he tenido en cuenta, dado que 
     * requiere mas tiempo y dedicacion del que dispongo. En estos
     * casos, para evitar que los poligonos se deformen demasiado
     * o tengan un comportamiento extraño respecto a la textura, 
     * primero guardo los datos antes de la modificacion (puntos
     * del collider y textura) y si algo falla entonces los restauro,
     * asi en el peor de los casos no habra efecto de explosion en el 
     * terreno.
     */
    public void Explosion(PolygonCollider2D polygon, CircleCollider2D circle, float radius, List<Line2D> lines) {
        Vector2[] points = polygon.points;
        Texture2D texture = sr.sprite.texture;
        try {
            ExplosionCollider(polygon, circle, radius, lines);
            ExplosionTexture(circle, radius);
        } catch (Exception e) {
            polygon.points = points;
            sr.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one / 2);
        }
    }



    /**
     * Metodo para modificar la textura del terreno tras la explosion.
     * 
     * Primero, obtiene las posiciones del circulo en el espacio local
     * del terreno, para poder obtener el "rectangulo" de pixeles de 
     * la textura del terreno afectado por el radio del circulo.
     * 
     * Luego itera sobre este pequeño rectangulo para modificar el alpha
     * de los pixeles y hacerlos transparentes, pero solo modifica un
     * pixel si no es transparente ya, y si esta dentro del radio
     * del circulo.
     * 
     * Asi se consigue que todos los pixeles de la textura del terreno 
     * que esten dentro del radio del circulo se hagan transparentes.
     */
    private void ExplosionTexture(CircleCollider2D circleCol, float radius) {
        Sprite sprite = sr.sprite;
        Bounds b = sprite.bounds;
        Vector2 offset = new Vector2(b.extents.x, b.extents.y);

        //circle in world space
        Vector2 circleCenter = circleCol.transform.position;

        //circle start of texture in terrain's local space
        Vector2 circleStart = circleCenter - Vector2.one * radius;
        Vector2 localCircleStart = tr.InverseTransformPoint(circleStart);
        localCircleStart += offset;
        //circle end of texture in terrain's local space
        Vector2 circleEnd = circleCenter + Vector2.one * radius;
        Vector2 localCircleEnd = tr.InverseTransformPoint(circleEnd);
        localCircleEnd += offset;

        //pixels start and end equivalent to local's circle start and end positions
        Vector2Int startPixels = new Vector2Int(
            Mathf.Max(0, (int)(localCircleStart.x / b.size.x * texture.width)),
            Mathf.Max(0, (int)(localCircleStart.y / b.size.y * texture.height)));
        Vector2Int endPixels = new Vector2Int(
            Mathf.Min(texture.width - 1, (int)(localCircleEnd.x / b.size.x * texture.width)),
            Mathf.Min(texture.height - 1, (int)(localCircleEnd.y / b.size.y * texture.height)));

        //for each pixel in this "square"
        for (int i = startPixels.y; i < endPixels.y; i++) {
            for (int j = startPixels.x; j < endPixels.x; j++) {
                //already transparent?
                if (texture.GetPixel(j, i).a == 0)
                    continue;
                //terrain's local position of pixel
                Vector2 localPos = new Vector2(
                    (float)j / texture.width * b.size.x - b.extents.x,
                    (float)i / texture.height * b.size.y - b.extents.y);
                //world's position of the pixel
                Vector2 worldPos = tr.TransformPoint(localPos);
                //outside of the circle?
                if (Vector2.Distance(worldPos, circleCenter) > radius)
                    continue;
                //set pixel transparent if is inside the circle
                Color c = texture.GetPixel(j, i);
                c.a = 0;
                texture.SetPixel(j, i, c);
            }
        }
        texture.Apply();
        sr.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one / 2);
    }



    /**
     * Metodo para modificar el collider del terreno segun la explosion.
     * 
     * Primero, crea una lista con todos los angulos que tendra que
     * poner en esa parte del collider del terreno (cuantos mas angulos mas 
     * redondo se vera y menos poligonal, pero mas costoso computacionalmente).
     * 
     * Segundo, busca una linia que quede fuera del terreno para despues
     * ordenar las linias a partir de esa, ya que para comprobaciones
     * posteriores, una interseccion con el terreno (desde la primera 
     * linia que entra al terreno por un lado hasta la primera que sale por el otro)
     * no puede empezar al final de la lista y terminar al principio,
     * deben estar todas las linias de una interseccion juntas.
     * 
     * Tercero, separa por listas todas las intersecciones.
     * 
     * Cuarto, busca el vertice del collider del terreno que mas alejado
     * se encuentre del circulo (el vertice en el otro extremo del collider 
     * del terreno) para asegurar que dicho vertice no esta dentro del radio 
     * de explosion o que esta entre dos intersecciones con la esfera,
     * ya que luego itera sobre todos los vertices y es necesario que 
     * empiece desde fuera del circulo.
     * Encuentra el centro del collider poligonal para obtener la direccion 
     * contraria al circulo y asi conseguir un punto muy alejado del circulo
     * en el espacio, pasando por el centro del poligono. Luego busca el
     * vertice del collider del terreno que mas cerca quede y lo guarda como 
     * el mas lejano. Asi se consigue un vertice sobre el que iterar asegurando
     * que dicho vertice esta fuera del circulo o entre dos intersecciones
     * con el circulo.
     * 
     * Y finalmente, itera sobre todos los vertices del collider del terreno
     * poniendolos todos en una nueva lista (los que no estan dentro del 
     * circulo) hasta que encuentra uno que SI esta dentro del circulo, 
     * entonces pone en la nueva lista de vertices la primera interseccion 
     * de puntos con el circulo. Hace lo mismo con todas las intersecciones
     * y al acabar le da esta nueva lista de vertices al collider del terreno.
     * 
     * Es decir, que por cada grupo de vertices del terreno que estan dentro 
     * del circulo, son substituidos por los nuevos vertices creados que 
     * siguen el contorno del circulo, y que automaticamente enlazan con los
     * vertices del terreno que estan a cada extremo de cada interseccion
     * con el circulo.
     * 
     * (Esta implementacion (de momento) hace que siempre tenga que haber 
     * vertices dentro del circulo, por lo que el PolygonCollider2D del 
     * terreno debe ser generoso con los vertices iniciales en la superficie,
     * y el radio del circulo debe ser mas grande que la distancia minima
     * entre dos vertices del terreno)
     */
    private void ExplosionCollider(PolygonCollider2D polygonCol, CircleCollider2D circleCol, float radius, List<Line2D> lines) {
        //check if polygon is completely inside the circle,
        //then destroy completely.
        bool anyInside = false;
        for (int i = 0; i < lines.Count; i++) {
            if (polygonCol.OverlapPoint(lines[i].point)) {
                anyInside = true;
                break;
            }
        }
        if (!anyInside) {
            polygons.Remove(polygonCol);
            Destroy(polygonCol.gameObject);
            return;
        }
        

        //get first line found outside the terrain
        int idx = -1;
        for (int i = 0; i < lines.Count; i++) {
            if (!polygonCol.OverlapPoint(lines[i].point)) {
                idx = i;
                break;
            }
        }
        

        //orders the line's list to avoid "cuts" (lines
        //touching the terrain's collider must be one after
        //the other)
        List<Line2D> linesOrdered = new List<Line2D>();
        for (int i = 0; i < lines.Count; i++) {
            if (idx == lines.Count)
                idx = 0;
            linesOrdered.Add(lines[idx]);
            idx++;
        }
        

        //puts together all intersections (from the first line 
        //touching Terrain to the last), because there can be 
        //more than one intersections with terrain's collider
        List<List<Vector2>> intersectedParts = new List<List<Vector2>>();
        List<Vector2> aux = new List<Vector2>();
        foreach (Line2D line in linesOrdered) {
            if (polygonCol.OverlapPoint(line.point)) { //looking for intersection
                aux.Add(line.point);
            } else if (aux.Count > 0) { //looking for the end of intersection
                intersectedParts.Add(new List<Vector2>(aux));
                aux.Clear();
            }
        }
        if (aux.Count > 0) { //looking for the end of intersection
            intersectedParts.Add(new List<Vector2>(aux));
        }
        

        //find farest terrain's collider point to start the 
        //iteration over its points.
        //finds the center of the polygon collider to get 
        //the opposite direction to the circle
        int farestIdx = 0, totalAmount = 0;
        Vector2[] points = polygonCol.points,
            worldPoints = new Vector2[points.Length];
        Vector2 center = Vector2.zero;
        Vector3 circleCenter = circleCol.transform.position;
        for (int i = 0; i < points.Length; i++) {
        Vector2 p = points[i];
            worldPoints[i] = tr.TransformPoint(p);
            if (circleCol.OverlapPoint(worldPoints[i])) {
                center += worldPoints[i];
                totalAmount++;
            }
        }
        center = center / totalAmount;
        

        Vector2 farPoint = circleCenter + ((Vector3)center - circleCenter) * 100;
        Vector2 farest = worldPoints[0];
        float farestDist = Vector2.Distance(farPoint, farest);
        for (int i = 1; i < worldPoints.Length; i++) { //find farest
            Vector2 p = worldPoints[i];
            float dist = Vector2.Distance(farPoint, p);
            if (dist < farestDist && !circleCol.OverlapPoint(p)) {
                farest = p;
                farestDist = dist;
                farestIdx = i;
            }
        }
        

        //adding the new points created by the circle
        //to the terrain's collider
        List <Vector2> list = new List<Vector2>();
        int intersectedPartsIdx = 0;
        bool alreadyIn = false;
        for (int i = 0; i < points.Length; i++) {
            if (farestIdx == points.Length)
                farestIdx = 0;
            Vector2 p = worldPoints[farestIdx];
            if (!circleCol.OverlapPoint(p)) {
                list.Add(points[farestIdx]);
                alreadyIn = false;
            } else if (!alreadyIn) {
                foreach (Vector2 intersected in intersectedParts[intersectedPartsIdx])
                    list.Add(tr.InverseTransformPoint(intersected));
                intersectedPartsIdx++;
                alreadyIn = true;
            }
            farestIdx++;
        }

        polygonCol.points = list.ToArray();

        points = polygonCol.points;
        worldPoints = new Vector2[points.Length];
        Vector2 prev = points[0],
            worldPrev = tr.TransformPoint(prev);

        List<Vector2> futureCol1 = new List<Vector2>(),
                      futureCol2 = new List<Vector2>();
        List<Vector2> actualFutureCol = futureCol1;
        actualFutureCol.Add(prev);
        radius *= 0.95f;
        for (int i = 1; i < points.Length; i++) {
            Vector2 p = points[i];
            worldPoints[i] = tr.TransformPoint(p);
            Vector2 mid = (worldPoints[i] + worldPrev) / 2;
            float dist = Vector2.Distance(mid, circleCenter);
            if (dist < radius) {
                actualFutureCol = actualFutureCol == futureCol1 ? futureCol2 : futureCol1;
            }
            actualFutureCol.Add(p);
            prev = p;
            worldPrev = worldPoints[i];
        }
        if (futureCol2.Count > 0) {
            polygonCol.points = futureCol1.ToArray();
            GameObject newChild = new GameObject("NewPolygon (" + polygons.Count + ")");
            newChild.layer = LayerMask.NameToLayer("Terrain");
            Transform newChildTr = newChild.transform;
            newChildTr.SetParent(transform);
            newChildTr.localPosition = Vector3.zero;
            PolygonCollider2D newCol = newChild.AddComponent<PolygonCollider2D>();
            newCol.points = futureCol2.ToArray();
            polygons.Add(newCol);
        }
    }

}
