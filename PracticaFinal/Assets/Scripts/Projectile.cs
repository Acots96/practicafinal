﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase para controlar el comportamiento relativo a un proyectil y su informacion.
 * 
 * Dado que hay dos tipos de proyectiles, el basico y el propio de cada personaje,
 * tiene un enum para distinguirlos, ya que el basico es el mismo para todos pero
 * cada personaje tiene un tipo de proyectil especial:
 *  - Blue: Proyectil antigravitatorio. Tiene el mismo comportamiento que el basico
 *  pero eliminando la gravedad del Rigidbody2D.
 *  - Brown: Lluvia de proyectiles. Mismo comportamiento que el basico pero siendo
 *  multiples disparos utilizando una Coroutine para utilizar un tiempo (aleatorio)
 *  de separacion entre ellos y un rango determinado de angulos (aleatorios tambien).
 *  - Pink: Conjunto de 3 proyectiles que caen del cielo hacia las posiciones de los
 *  otros personajes (aproximadamente, no la posicion exacta).
 *  - Green: Bomba magnetizada. Mismo comportamiento que el proyectil basico pero
 *  comprobando constantemente si hay otros personajes cerca, dentro de un radio 
 *  determinado. Si es asi cambia el movimiento parabolico por moverse directamente
 *  hacia ese personaje.
 */
public class Projectile : MonoBehaviour {

    public enum Type { BASE, BLUE, PINK, BROWN, GREEN }
    private Type type;

    public float ExplosionRadius;
    public GameObject ExplosionPrefab, SmokeTrailPrefab;
    private GameObject trail;

    private int NumOfAngles;
    private LayerMask CollisionMask, TerrainMask;

    private Transform tr;
    private Rigidbody2D rb;
    private CircleCollider2D circleCol;
    private SpriteRenderer sr;
    private CharacterController character;

    private bool hasExploded, greenEffectDone, justInstantiated;


    private void Awake() {
        tr = transform;
        rb = GetComponent<Rigidbody2D>();
        circleCol = GetComponent<CircleCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }


    /**
     * Metodo llamado para ejecutar el lanzamiento del proyectil, dando toda la 
     * informacion necesaria:
     *  - CharacterController que ha lanzado el proyectil (necesario al explotar)
     *  - Fuerza con la que se lanza.
     *  - Numero de angulos con los que hara el calculo al chocar contra un 
     *  Collider de terreno (mas detalles en TerrainController)
     *  - LayerMask de collision, para saber con que layers puede colisionar.
     *  - LayerMask del terreno, para saber su layer
     *  - Type para saber el tipo de proyectil lanzado.
     */
    public void Shoot(CharacterController c, float force, int angles, LayerMask collision, LayerMask terrain, Type t) {
        character = c;
        NumOfAngles = angles;
        CollisionMask = collision;
        TerrainMask = terrain;
        type = t;
        Vector2 direction = tr.up;
        if (type == Type.BLUE) {
            rb.gravityScale = 0;
        } else if (type == Type.PINK) {
            rb.gravityScale = 0.3f;
        }
        rb.AddForce(direction * force, ForceMode2D.Impulse);
        trail = Instantiate(SmokeTrailPrefab, tr.position, Quaternion.identity);
        trail.transform.SetParent(tr);
        justInstantiated = true;
        Invoke("NotJustInstantiated", 0.3f);
    }


    /**
     * Metodo para actualizar la situacion del proyectil.
     * 
     *  - Mientras no haya explotado hace que apunte hacia donde se mueve.
     *  - Si es el proyectil del personaje verde debe comprobar si hay personajes
     * cerca, y si es asi dirigirse hacia el mas cercano.
     *  - Si es azul, al no caer, hay que controlar la distancia vertical para 
     *  que no se aleje indefinidamente en caso de ser disparado hacia arriba.
     */
    void Update() {
        if (!hasExploded) {
            tr.up = rb.velocity.normalized;
        }
        if (type == Type.GREEN && !greenEffectDone) {
            Transform closeChar = character.IsOtherCharacterClose(tr.position);
            if (closeChar) {
                rb.gravityScale = 0;
                rb.velocity = (closeChar.position - tr.position).normalized * 5;
                greenEffectDone = true;
            }
        }
        if (type == Type.BLUE) {
            if (Mathf.Abs(tr.position.y - character.transform.position.y) > 40)
                Explode();
        }
    }



    /**
     * Metodo para hacer explotar el proyectil.
     * 
     *  - Obtiene la informacion que necesita el terreno para simular la explosion
     * (mas detalles en TerrainController).
     *  - Obtiene todos los colliders que esten dentro del radio de explosion, ya
     *  sean terrenos para destruir (PolygonCollider2D) o personajes para 
     *  inflingir daño (CharacterController).
     *  - Finalmente instancia el efecto de la explosion y espera justo el tiempo 
     *  que este dura para destruir el mismo y el efecto de trail, activo desde
     *  el mismo lanzamiento del proyectil.
     */
    private void Explode() {
        hasExploded = true;
        ExplosionRadius = character.ExplosionRange;
        circleCol.radius = ExplosionRadius / tr.localScale.x;
        float radius = ExplosionRadius;
        if (type == Type.PINK)
            radius *= 0.85f;
        else if (type == Type.BROWN)
            radius *= 0.6f;
        //creates as much lines as NumOfAngles
        List<Line2D> lines = new List<Line2D>();
        Vector2 circleCenter = transform.position;
        float pi2 = Mathf.PI * 2;
        float radsIncrement = pi2 / NumOfAngles;
        for (float rads = 0; rads <= pi2; rads += radsIncrement) {
            Vector2 dir = new Vector2(Mathf.Cos(rads), Mathf.Sin(rads)) * radius;
            Line2D l = new Line2D(circleCenter, circleCenter + dir);
            lines.Add(l);
        }

        int points = 0;

        Collider2D[] overlaped =
            Physics2D.OverlapCircleAll(tr.position, radius, CollisionMask.value);
        foreach (Collider2D c in overlaped) {
            CharacterController cc = c.GetComponent<CharacterController>();
            if (c is PolygonCollider2D) {
                if (type != Type.BROWN) {
                    TerrainController tc = c.GetComponentInParent<TerrainController>();
                    tc.Explosion(c as PolygonCollider2D, circleCol, radius, new List<Line2D>(lines));
                }
            } else if (cc != null && circleCol.OverlapPoint(cc.transform.position)) {
                float dmg = character.Damage;
                if (type == Type.PINK)
                    dmg *= 0.5f;
                else if (type == Type.BROWN)
                    dmg *= 0.15f;
                points += cc.TakeDamage(circleCol, radius, (int)dmg);
            }
        }

        circleCol.enabled = sr.enabled = false;
        float duration = ExplosionPrefab.GetComponent<ParticleSystem>().main.duration;
        //Destroy(trail, duration);
        trail.transform.SetParent(null);
        Destroy(Instantiate(ExplosionPrefab, tr.position, Quaternion.identity), duration);
        character.Exploded(points);
        Destroy(gameObject);
    }



    /**
     * Al chocar contra algo comprueba que sea uno de los layers esperados
     * para poder explotar, como 'Character' o 'Terrain'.
     * Para evitar que choque con el personaje justo al ser instanciado
     * comprueba con quien choca y si acaba de ser instanciado.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.collider.gameObject.Equals(character.gameObject) && justInstantiated)
            return;
        int layer = collision.collider.gameObject.layer;
        if (!hasExploded && (CollisionMask.value & 1 << layer) != 0) {
            Explode();
        }
    }


    /**
     * Metodo llamado al cabo de 0.3 segundos de ser instanciado para
     * permitirle chocar, ya que al ser instanciado puede chocar con el 
     * personaje que lo ha instanciado y explotar, de esta forma espera
     * un pequeño espacio de tiempo para poder tener en cuenta una colision
     * con el propio personaje emisor.
     */
    private void NotJustInstantiated() {
        justInstantiated = false;
    }





    /**
     * Struct para guardar un conjunto de informacion
     * requerida en el metodo ExplosionCollider
     */
    public struct Line2D {
        public Vector2 center, point, direction;
        public Collider2D collider;
        public Line2D(Vector2 c, Vector2 p, Collider2D col) {
            center = c;
            point = p;
            direction = (p - c).normalized;
            collider = col;
        }
        public Line2D(Vector2 c, Vector2 p) {
            center = c;
            point = p;
            direction = (p - c).normalized;
            collider = null;
        }
    }

}
