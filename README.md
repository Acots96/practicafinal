# PEC4 - Práctica Final


*KaBoom!* es un juego que consiste en disparar proyectiles a tus enemigos por turnos con un máximo de 4 jugadores (mínimo 1).
(En el juego siempre hay 4 personajes, por lo que todos los que no estén ocupados por una persona utilizarán IA para disparar a sus enemigos).


# Escenas

La primera escena es la TitleScene con el logo de la UOC y el título del juego.
Tiene un fadein-fadeout del logo de la UOC primero y del título del juego en segundo lugar. Pulsando cualquier tecla o el mouse se puede saltar.

La segunda escena es la MenuScene, con las distintas opciones (botones de opciones, créditos, HowToPlay y Play) y añadir jugadores con sus personajes. 
 - Para añadir un jugador primero el usuario deberá pulsar el último botón disponible de la columna de la izquierda (botón azul sin imagen de personaje) y luego pulsar sobre uno de los personajes disponibles de la columna de la derecha (los personjaes disponibles son botones con el fondo azulelegir un personaje el jugador deberá pulsar el último botón disponible de la columna de la izquierda (jugadores) y luego elegir uno de los personajes disponibles en la columna de la derecha.
 - Para modificar cambiar el personaje de un jugador se debe repetir la misma opción: pulsar sobre el botón del jugador de la columna de la izquierda y luego pulsar sobre un personaje disponible de la columna derecha.
 - Para quitar un jugador el usuario deberá pulsar el botón con forma de X que tienen al lado los botones de la columna izquierda. Puesto que hay un orden, solo se podrá eliminar el último jugador que se haya añadido excepto el primero, ya que debe haber almenos un jugador para poder jugar.
 - Para ver los atributos de cada personaje el usuario deberá pasar el ratón por encima de los personajes de la columna de la derecha y se mostrarán en la parte central.

Además de los principales (*Options* y *Credits*), contiene un botón HowToPlay el cual muestra al usuario una escena del juego con sus partes y las instrucciones sobre como jugar.

La tercera escena es la GameScene, donde se desarrolla la partida.
 - En la parte superior está el tiempo que dura un turno (barra verde y contador debajo) y el que dura la partida (contador a la derecha, modificable en el inspector de GameManger).
 - En la parte inferior izquierda están los 4 personajes. Tienen el número de jugador (o "IA" para los que no sean jugadores), los puntos conseguidos debajo del nombre y la barra de vida debajo de la imagen del personaje.
 - En la parte inferior derecha están las dos armas de los personajes: a la izquierda la básica (con munición ilimitada) y a la derecha la especial de cada uno (máximo 3 veces). Por defecto está seleccionada siempre la básica, pero el jugador puede seleccionar la que desee (siempre que tenga munición en el caso de la especial).
 - En la parte inferior central sale indicada la fuerza de disparo del jugador que está apuntando para disparar.
 
Finalmente están las escenas:
 - CreditsScene, con la información de los créditos.
 - OptionsScene, para regular el volumen de los efectos y de las canciones, el cual se guarda aunque se cierre la partida utilizando PlayerPrefs. También tienen un botón de "Play" al lado derecho para poder testear el volumen de ambos. Para poder parar la canción, el botón de "Play" de la música se transforma en uno de pausa para poder pausarla.
 - HowToPlayScene, con una imagen de una partida e indicaciones sobre todos sus componentes, además de unas breves indicaciones sobre como jugar.
 
Todos los elementos de las UIs han sido cuidadosamente ubicados, adaptando todos y cada uno de sus *Anchors* para que la UI completa se adapte a la pantalla. Sin embargo es importante poner el aspect ratio de la ventana Game en 3:2
 
 
# Jugabilidad

El juego funciona por turnos de 20 segundos, eligiendo aleatoriamente quien empieza para que no empiece siempre por orden. El turno acaba cuando pasan los 20 segundos o cuando el jugador dispara su proyectil.
Cuando es el turno de un jugador debe utilizar el botón izquierdo del ratón para apuntar hacia donde quiera disparar y la rueda central del ratón para incrementar/decrementar la potencia de tiro. Para ello contará con la ayuda visual de una flecha blanca que se moverá alrededor del personaje apuntando hacia donde el usuario esté clicando con el mouse. Esta cambiará de color y tamaño según el movimiento de la rueda del mouse.
Para cancelar el disparo (mientras se está apuntando) el usuario puede pulsar la tecla ESCAPE, lo que hará que la flecha desaparezca y el tiro quede cancelado. 
El jugador puede moverse y saltar por el escenario los segundos que duren su turno, incluso puede apuntar mientras se mueve, pero solo puede disparar una vez.
La partida finaliza exactamente al finalizar el tiempo de juego, sino cuando además también ha acabado el turno actual, para dar al personaje que está jugando la posibilidad de acabar su movimiento. Al finalizar aparece una tabla con los cuatro personajes ordenados por sus puntuaciones de arriba a abajo, y permite volver a la escena de menú MenuScene, donde se resetea todo y se puede jugar otra partida.

Los proyectiles especiales de los personajes son los siguientes:
 - Azul: Proyectil antigravitatorio (apuntar directamente al enemigo sin necesidad de pensar en una trayectoria parabólica, ya que este no se ve afectado por la gravedad).
 - Rosa: Proyectiles desde el cielo (sin necesidad de apuntar, solo clicar en la pantalla ya que aparecen 3 proyectiles que caen del cielo directamente hacia sus oponentes).
 - Marrón: Lluvia de proyectiles (apuntar y disparar igual que el arma básica pero dispara 6 proyectiles pequeños).
 - Verde: Bomba magnetizada (apuntar y disparar igual que el arma básica pero cuando la bomba esté lo suficientemente cerca de otro personaje cambiará su dirección para ir directamente hacia dicho personaje, como si de un imán se tratara).

Todos los personajes tienen el mismo funcionamiento exceptuando los sprites, ya que cada uno usa un grupo de sprites distinto. También cada uno tiene sus dos prefabs de los proyectiles.


Los proyectiles han estado guardados como prefabs, siendo el proyectil base el mismo prefab para todos (al no ser distinto). Sin embargo, como los especiales difieren en el dibujo han sido guardados como prefabs distintos. 
Los 5 prefabs tienen su efecto de explosión y trail distinto, para los cuales he utilizado como base los efectos de particulas 5.X de la Asset Store de Unity y los he modificado ligeramente para obtener los resultados que yo quería, haciendo cada uno específico para cada proyectil. Para ello también he añadido una luz direccional en la escena, ya que los efectos contienen elementos 3D que se ven completamente oscuros sin una luz que los enfoque.

También hay sonidos de efectos para prácticamente todas las acciones que el usuario pueda realizar, ya sea al interaccionar con elementos de la UI o mientras está jugando una partida. También hay algún efecto de sonido específico, como uno parecido a un timbre cuyo objetivo es llamar la atención del jugador al empezar un nuevo turno, u otro que simula la cuenta atrás cuando quedan 3 segundos del turno para avisar al jugador. Además tiene dos canciones de ambiente en las escenas MenuScene y GameScene. Al finalizar una partida se para la música y suena un efecto de sonido específico, típico de este momento en este tipo de juegos con varios personajes y puntuaciones.

La destrucción del terreno ha sido una de las partes más complejas, puesto que debe modificar un PolygonCollider2D y debe tener en cuenta muchos pequeños aspectos y casos posibles, como la separación de un trozo de terreno en dos, y por lo tanto la creación de otro PolygonCollider2D en la otra zona, entre otros ejemplos. Sin embargo, debido a su la complejidad, además de algunos pocos casos aislados y poco frecuentes, al lanzar varios proyectiles seguidos en una misma zona surgen más problemas de los que deberían, por lo que he decidido que la lluvia de proyectiles del personaje Brown no afecte al terreno, para evitar una mala experiencia de juego.


*(Para más detalles sobre la implementación del código ver las cabeceras de las clases y los métodos)*


Enlace del video: https://youtu.be/g7kmW6p7u2s



