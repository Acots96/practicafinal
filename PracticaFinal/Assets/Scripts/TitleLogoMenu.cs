﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase de la primera escena, con un simple algoritmo para hacer un 
 * efecto fade in y fade out del logo de la UOC primero y del nombre
 * del juego en segundo lugar.
 * (se puede saltar pulsando cualquier tecla o boton del raton)
 */
public class TitleLogoMenu : MonoBehaviour {

    [SerializeField]
    private Image LogoImage;
    [SerializeField]
    private Text TitleText;


    private void Awake() {
        StartCoroutine(TitleLogoEffect());
    }


    private void Update() {
        if (Input.anyKeyDown)
            SceneManager.LoadScene("MenuScene");
    }



    private IEnumerator TitleLogoEffect() {
        Color startColor = LogoImage.color;
        Color endColor = Color.white;
        float progress = 0, multiplier = 0.75f;
        yield return new WaitForSeconds(1f);
        while (progress <= 1) {
            progress += Time.deltaTime * multiplier;
            LogoImage.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        while (progress >= 0) {
            progress -= Time.deltaTime * multiplier;
            LogoImage.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }
        while (progress <= 1) {
            progress += Time.deltaTime * multiplier;
            TitleText.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        while (progress >= 0) {
            progress -= Time.deltaTime * multiplier;
            TitleText.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }
        SceneManager.LoadScene("MenuScene");
    }
}
