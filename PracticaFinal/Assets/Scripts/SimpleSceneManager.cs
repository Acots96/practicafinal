﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Clase para controlar las escenas de creditos y de HowToPlay,
 * ya que ambas contienen unicamente el boton de volver a MenuScene
 */
public class SimpleSceneManager : MonoBehaviour {

    [SerializeField]
    private AudioSource AcceptSound;

    public void BackToMenu() {
        AcceptSound.volume = GameObject.Find("GameManager").GetComponent<GameManager>().EffectsVolume;
        AcceptSound.Play();
        SceneManager.LoadScene("MenuScene");
    }
}
