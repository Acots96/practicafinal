﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase que controla todo el funncionamiento de una partida.
 * 
 * Contiene toda la informacion relativa a una partida, incluyendo 
 * los personajes, la UI y la comunicacion con el GameManager
 */
public class PlayManager : MonoBehaviour {

    [SerializeField]
    private float TurnTime;
    [SerializeField]
    private Image TimeBar;
    [SerializeField]
    private Text TimeText, GameTimeText, StartCountdownText;
    [SerializeField]
    private SlotCharacterInfo[] slots;
    [SerializeField]
    private CharacterController[] controllers;
    [SerializeField]
    private Color ThisTurnColor, NormalColor;
    [SerializeField]
    private Transform SafeRespawnPoints;

    [SerializeField]
    private Button BaseWepButton, SpecialWepButton;
    [SerializeField]
    private Image BaseWepButtonImage, SpecialWepButtonImage, SpecialWepImage;
    [SerializeField]
    private Text SpecialWepUnitsText;
    public Text ShootingForceText;

    [SerializeField]
    private Image BackgroundEndGameImage;
    [SerializeField]
    private Image[] CharsImages;
    [SerializeField]
    private Text[] CharsPointsTexts;
    [SerializeField]
    private Button GoMenuButton;


    public Transform UpOutsidePosition;
    
    private GameManager manager;
    private List<GameManager.CharacterInfo> charsInfo;
    private int actualPlayerIdx, totalTimeInSeconds;
    private bool isGameOver;

    public int SelectedWeapon { get; private set; }


    private void Awake() {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        manager.playManager = this;

        UpdateVolume();
        GameSongSound.Play();

        charsInfo = new List<GameManager.CharacterInfo>(manager.characters);
        for (int i = 0; i < charsInfo.Count; i++) {
            GameManager.CharacterInfo c = charsInfo[i];
            c.controller = controllers[i];
            CharacterController cc = c.controller;
            cc.isPlayer = c.isPlayer;
            cc.SpeedMultiplier = manager.GetSpeedAsNum(c.speed);
            cc.Damage = c.damage;
            cc.ExplosionRange = c.explosionRange;
            cc.slot = slots[i];
            cc.slot.Name = c.isPlayer ? "Player "+(c.playerNum+1) : "AI";
            cc.slot.Points = 0;
            cc.slot.Life = c.healthPoints;
        }

        actualPlayerIdx = Random.Range(0, slots.Length);
        isGameOver = false;
        StartCoroutine(DoTurns());
        totalTimeInSeconds = manager.GameTimeInSeconds;
        int minutes = totalTimeInSeconds / 60;
        int seconds = manager.GameTimeInSeconds % 60;
        string secs = seconds < 10 ? "0" + seconds : seconds + "";
        GameTimeText.text = minutes + ":" + secs;
    }



    /**
     * Metodo llamado cada vez que el personaje selecciona un arma.
     * En caso de ser la especial siempre comprueba si tiene unidades
     * suficientes, y si no es asi mantiene seleccionada el arma basica.
     */
    public void SelectWeapon(int w) {
        PlaySoundEffect(Effects.ACCEPT);
        if (w == 1 && controllers[actualPlayerIdx].SpecialWepUnits == 0)
            w = 0;
        SelectedWeapon = w;
        BaseWepButtonImage.color = w == 0 ? ThisTurnColor : NormalColor;
        SpecialWepButtonImage.color = w == 0 ? NormalColor : ThisTurnColor;
    }


    /**
     * Metodo para que quede seleccionada el arma basica antes de cada 
     * turno y resetee la informacion relacionada.
     */
    private void ResetSelectedWeapon() {
        CharacterController c = controllers[actualPlayerIdx];
        SelectedWeapon = 0;
        BaseWepButtonImage.color = ThisTurnColor;
        SpecialWepButtonImage.color = NormalColor;
        SpecialWepImage.sprite = c.GetSpecialWepSprite();
        SetSpecialWepUnits(c.SpecialWepUnits);
        BaseWepButton.interactable = SpecialWepButton.interactable = c.isPlayer;
    }
    public void SetSpecialWepUnits(int specialWepUnits) {
        SpecialWepUnitsText.text = "Units: x" + specialWepUnits;
        SpecialWepButtonImage.color = NormalColor;
    }

    
    public void SetShootingForceText(int f) {
        ShootingForceText.text = f + "";
    }



    /**
     * Metodo llamado cada vez que un personaje muere y debe reaparecer.
     * Obtiene un punto de respawn seguro, lo que significa que tiene terreno
     * debajo y no detecta ningun otro personaje.
     */
    public Transform GetSafeRespawnPoint() {
        List<Transform> aux = new List<Transform>(SafeRespawnPoints.childCount);
        foreach (Transform safe in SafeRespawnPoints)
            aux.Add(safe);
        while (aux.Count > 0) {
            int idx = Random.Range(0, aux.Count);
            Transform tr = aux[idx];
            RaycastHit2D hit = Physics2D.Raycast(tr.position, Vector2.down, 100, manager.TerrainMask);
            if (hit.transform != null && hit.transform.gameObject.layer != LayerMask.NameToLayer("Character")) {
                Transform t = new GameObject().transform;
                t.position = hit.point + Vector2.up * 2;
                return t;
            }
            aux.RemoveAt(idx);
        }
        return null;
    }



    public void GameOver() {
        isGameOver = true;
    }


    /**
     * Metodo llamado al finalizar la partida, encargado de mostrar 
     * el cuadro con los personajes ordenados por sus puntos.
     */
    public void ShowEndGame() {
        GameSongSound.Stop();
        PlaySoundEffect(Effects.FINISHGAME);
        List<CharacterController> chars = new List<CharacterController>(controllers);
        chars.Sort((cc1, cc2) => cc2.slot.Points.CompareTo(cc1.slot.Points));
        for (int i = 0; i < chars.Count; i++) {
            CharsImages[i].sprite = chars[i].slot.CharacterImage.sprite;
            CharsPointsTexts[i].text = (i + 1) + ". " + chars[i].slot.Points + " (" + chars[i].slot.Name + ")";
        }
        BackgroundEndGameImage.gameObject.SetActive(true);
        GoMenuButton.gameObject.SetActive(true);
    }
    public void GoMenu() {
        PlaySoundEffect(Effects.ACCEPT);
        SceneManager.LoadScene("MenuScene");
    }



    /**
     * Metodo llamado cada segundo mediante el InvokeRepeating para actualizar
     * el contador global de la partida.
     */
    private void GameTime() {
        int minutes = totalTimeInSeconds / 60;
        int seconds = totalTimeInSeconds % 60;
        string secs = seconds < 10 ? "0" + seconds : seconds + "";
        GameTimeText.text = minutes + ":" + secs;
        if (minutes == 0 && seconds == 0)
            GameOver();
        else
            totalTimeInSeconds--;
    }



    /**
     * Corutina principal, encargada de controlar el flujo de la partida.
     * 
     *  - Primero pone los personajes en el terreno aleatoriamente.
     *  - Segundo inicia la cuenta atras.
     *  - Tercero ejecuta el flujo principal del juego, encargado de 
     *  cambiar de un turno a otro y actualizar el contador de tiempo del turno.
     *  - Finalmente, al acabar, muestra el la tabla de puntos.
     */
    private IEnumerator DoTurns() {
        List<Transform> aux = new List<Transform>(SafeRespawnPoints.childCount);
        foreach (Transform safe in SafeRespawnPoints)
            aux.Add(safe);
        foreach (CharacterController cc in controllers) {
            int idx = Random.Range(0, aux.Count);
            cc.transform.position = aux[idx].position;
            aux.RemoveAt(idx);
        }
        
        float time = 3;
        int countdown = 3;
        while (time > 0) {
            int timeInt = (int)(time + 1);
            if (timeInt <= countdown) {
                countdown--;
                PlaySoundEffect(Effects.ENDINGCOUNTDOWN);
            }
            StartCountdownText.text = timeInt + "";
            time -= Time.deltaTime;
            yield return null;
        }

        StartCountdownText.gameObject.SetActive(false);
        InvokeRepeating("GameTime", 0, 1);
        Transform timeBarTr = TimeBar.transform;
        while (!isGameOver) {
            CharacterController c = controllers[actualPlayerIdx];
            ResetSelectedWeapon();
            StartCoroutine(AnnounceTurnEffect(c));
            c.EnterTurn();
            float t = Time.time + TurnTime;
            countdown = 3;

            while (t > Time.time && !c.HasFinished()) {
                float fraction = (t - Time.time) / TurnTime;
                Vector3 scale = timeBarTr.localScale;
                scale.x = fraction;
                timeBarTr.localScale = scale;
                int timeInt = (int)(t - Time.time + 1);
                if (timeInt <= countdown) {
                    countdown--;
                    PlaySoundEffect(Effects.ENDINGCOUNTDOWN);
                }
                TimeText.text = timeInt + "";
                yield return null;
            }

            yield return new WaitForSeconds(1f);
            ShootingForceText.text = "";
            c.ExitTurn();
            yield return new WaitWhile(() => c.HasShooted());
            actualPlayerIdx = actualPlayerIdx == controllers.Length - 1 ? 0 : actualPlayerIdx + 1;
            timeBarTr.localScale = Vector3.one;
            TimeText.text = TurnTime + "";
            yield return null;
        }
        ShowEndGame();
    }

    /**
     * Corutina para mostrar el nombre del jugador que debe jugar.
     * Solo el de los jugadores, no el de los personajes controlados
     * por la IA.
     */
    private IEnumerator AnnounceTurnEffect(CharacterController c) {
        PlaySoundEffect(Effects.STARTTURN);
        if (c.isPlayer) {
            StartCountdownText.text = c.slot.Name + ", GO!";
            StartCountdownText.gameObject.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            StartCountdownText.gameObject.SetActive(false);
        }
        yield return null;
    }





    ////// SOUND
    
    // Todos los sonidos que hay durante la partida.
    public enum Effects { ACCEPT, SHOOT, BLUESHOOT, BROWNSHOOT, PINKSHOOT, GREENSHOOT, EXPLOSION, RUN, JUMP,
        HIT, FLIP, STARTTURN, FINISHGAME, ENDINGCOUNTDOWN, DIE, STEP1, STEP2 }

    [SerializeField]
    private AudioSource GameSongSound;
    public AudioSource AcceptSound, ShootSound, BlueShootSound, BrownShootSound, PinkShootSound, GreenShootSound,
        ExplosionSound, JumpSound, HitSound, FlipSound, DieSound, FinishGameSound, EndingCountdownSound, 
        StartTurnSound, GrassStepSound, ConcreteStepSound;

    /**
     * Metodo para actualizar el volumen cada vez que entra a la partida
     */
    private void UpdateVolume() {
        GameSongSound.volume = manager.MusicVolume;
        ShootSound.volume = ExplosionSound.volume = FlipSound.volume = JumpSound.volume = AcceptSound.volume =
            HitSound.volume = DieSound.volume = BlueShootSound.volume = BrownShootSound.volume =
            PinkShootSound.volume = GreenShootSound.volume = FinishGameSound.volume = StartTurnSound.volume =
            EndingCountdownSound.volume = GrassStepSound.volume = ConcreteStepSound.volume = manager.EffectsVolume;
    }

    /**
     * Metodo que unifica la ejecucion todos los sonidos, para que los
     * demas scripts solo deban preocuparse de llamarlo.
     */
    public void PlaySoundEffect(Effects e) {
        switch (e) {
            case Effects.ACCEPT:
                AcceptSound.Play();
                break;
            case Effects.SHOOT:
                ShootSound.Play();
                break;
            case Effects.BLUESHOOT:
                BlueShootSound.Play();
                break;
            case Effects.BROWNSHOOT:
                BrownShootSound.Play();
                break;
            case Effects.PINKSHOOT:
                PinkShootSound.Play();
                break;
            case Effects.GREENSHOOT:
                GreenShootSound.Play();
                break;
            case Effects.EXPLOSION:
                ExplosionSound.Play();
                break;
            case Effects.STEP1:
                // al modificar el pitch suena distinto, lo que
                // hace mas realista los pasos
                float randPitch1 = Random.Range(0.7f, 1.3f);
                ConcreteStepSound.pitch = randPitch1;
                ConcreteStepSound.Play();
                break;
            case Effects.STEP2:
                // al modificar el pitch suena distinto, lo que
                // hace mas realista los pasos
                float randPitch2 = Random.Range(0.7f, 1.3f);
                ConcreteStepSound.pitch = randPitch2;
                ConcreteStepSound.Play();
                break;
            case Effects.JUMP:
                JumpSound.Play();
                break;
            case Effects.HIT:
                HitSound.Play();
                break;
            case Effects.FLIP:
                FlipSound.Play();
                break;
            case Effects.STARTTURN:
                StartTurnSound.Play();
                break;
            case Effects.FINISHGAME:
                FinishGameSound.Play();
                break;
            case Effects.ENDINGCOUNTDOWN:
                EndingCountdownSound.Play();
                break;
            case Effects.DIE:
                DieSound.Play();
                break;
        }
    }

}
