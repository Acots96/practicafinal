﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Clase que guarda la informacion del slot de los personajes durante una partida.
 * 
 * Contiene todos los atributos de la UI (nombre, puntos, imagen y barra de vida)
 * y los metodos necesarios para modificarlos, ya sea al empezar una partida o 
 * durante la misma
 */
public class SlotCharacterInfo : MonoBehaviour {

    [SerializeField]
    private Text NameText, PointsText, SpecialWepUnitsText;
    [SerializeField]
    private Image LifeBarImage, BackgroundImage;
    public Image CharacterImage;
    [SerializeField]
    private Color NormalColor, ThisTurnColor;
    [SerializeField]
    private Button SpecialWepButton;

    private Color startColor;
    private new string name;
    private int points;
    private int startLifePts, actualLifePts;
    private bool changingPoints;


    public string Name {
        get { return name; }
        set { name = value; NameText.text = name; } }
    public int Points {
        get { return points; }
        set { points = value; PointsText.text = points + ""; } }
    public int Life {
        get { return actualLifePts; }
        set { startLifePts = value; actualLifePts = value; } }



    /**
     * Metodo ejecutado cuando es el turno de este personaje,
     * para actualizar el color de la imagen de fondo del slot
     * (un color mas claro para distinguirlo del resto).
     */
    public void ExecuteTurn() {
        BackgroundImage.color = ThisTurnColor;
    }

    /**
     * Metodo ejecutado cuando acaba el turno del personaje para
     * volver a poner el color como estaba.
     */
    public void ExitTurn() {
        BackgroundImage.color = NormalColor;
    }


    /**
     * Metodo llamado cada vez que hay que añadir o quitar puntos
     * al jugador, pero utilizando un efecto hace de contador para
     * que sea "suave" en lugar de poner directamente los puntos.
     */
    public void AddPoints(int p) {
        StartCoroutine(AddPointsEffect(p));
    }
    private IEnumerator AddPointsEffect(int p) {
        yield return new WaitWhile(() => changingPoints);
        changingPoints = true;
        int auxP = points;
        points = Mathf.Max(0, points + p);
        while (auxP != points) {
            auxP += (int)Mathf.Sign(p);
            PointsText.text = auxP + "";
            yield return new WaitForSeconds(0.02f);
        }
        changingPoints = false;
    }

    /**
     * Metodo para actualizar la vida segun el daño recibido.
     */
    public void TakeDamage(int dmg) {
        actualLifePts = Mathf.Max(0, actualLifePts - dmg);
        PointsText.text = points + "";
        Vector3 scale = LifeBarImage.transform.localScale;
        scale.x = (float)actualLifePts / startLifePts;
        LifeBarImage.transform.localScale = scale;
    }

    /**
     * Metodo llamado cuando muere el personaje para restablecer
     * la barra de vida
     */
    public void RestoreLife() {
        Vector3 scale = LifeBarImage.transform.localScale;
        scale.x = 1;
        LifeBarImage.transform.localScale = scale;
    }

}
