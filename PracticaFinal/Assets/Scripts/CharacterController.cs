﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase para controlar un jugador y su informacion, incluyendo la de su slot.
 */
public class CharacterController : MonoBehaviour {

    [SerializeField]
    private Animator animator;
    [SerializeField]
    private SpriteRenderer sr;
    [SerializeField]
    private Transform ArrowCenterTr, ArrowTr;
    [SerializeField]
    private SpriteRenderer ArrowSr;
    [SerializeField]
    public SlotCharacterInfo slot;

    private float MaxWheel;
    private Color MinColorArrow, MaxColorArrow;

    public float SpeedMultiplier, JumpForce, ExplosionRange;
    public int Damage;

    [SerializeField]
    private LayerMask layerMaskGround;
    [SerializeField]
    private Transform[] groundCheckers;
    [SerializeField]
    private Transform limitDownChecker;
    private float checkDistance;

    private GameManager manager;
    private GameObject[] WepsPrefabs;
    public int SpecialWepUnits { get; private set; }

    private Transform tr;
    private Rigidbody2D rb;
    private Vector3 normalScale, bigScale;

    private float speed, mouseWheel; //0.1 para arriba, -0.1 para abajo (+-0.2 si es rapido)
    private bool jump, click, hasClicked, hasLeftClick;
    private bool cancelClick;

    public bool isPlayer, isThisTurn, hasFinished, hasShooted;
    private bool isLoadingShoot, isFacingRight, canClick;

    private int projectilesLaunched, projectilesExploded;

    

    private void Awake() {
        isFacingRight = true;
        tr = transform;
        rb = GetComponent<Rigidbody2D>();
        rb.mass = 50;
        normalScale = ArrowTr.localScale;
        bigScale = normalScale * 2;
        SpecialWepUnits = 3;
        checkDistance = Mathf.Abs(limitDownChecker.transform.position.y - groundCheckers[0].transform.position.y);
    }



    private void Start() {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        JumpForce = manager.JumpForce;
        MaxWheel = manager.MaxWheel;
        MaxColorArrow = manager.MaxColorArrow;
        MinColorArrow = manager.MinColorArrow;
        WepsPrefabs = manager.GetWeaponsPrefabs(this);
    }



    /**
     * Si no es el turno del personaje entonces no continua, si lo es
     * entonces (solo si es un jugador) comprueba el input y realiza
     * las acciones oportunas (saltar, girarse, correr, etc.).
     * Cuando el jugador pulsa el raton (si puede) se ejecuta la 
     * corutina que se encarga de apuntar la flecha y disparar
     * (mas detalles en la cabecera de la propia corutina).
     */
    private void Update() {
        animator.SetFloat("SpeedY", rb.velocity.y);
        if (!isThisTurn)
            return;
        //if (isPlayer || true) {
        if (isPlayer) {
            CheckInput();
            if (speed != 0 && speed > 0 != isFacingRight)
                Flip();
            if (jump)
                Jump();
            if (hasClicked)
                click = true;
            else if (hasLeftClick)
                click = false;
            if (canClick && click) {
                canClick = false;
                StartCoroutine(LoadShootEffect());
            }
            //actualiza el animator al moverse lateralmente o caer
            animator.SetFloat("SpeedX", Mathf.Abs(speed));
            //animator.SetFloat("SpeedY", rb.velocity.y);
        }
    }
    /**
     * Dado que es el Rigidbody2D el que mueve al jugador
     * debe hacerlo desde el FixedUpdate.
     * Mueve al player segun la velocidad (que incluye el signo),
     * el tiempo transcurrido desde el ultimo frame (el cual hay que controlar
     * porque los primeros frames son demasiado largos) y un multiplicador 
     * para calibrar la velocidad desde el inspector.
     */
    private void FixedUpdate() {
        if (!isThisTurn || !isPlayer)
            return;
        float deltaTime = Time.fixedDeltaTime > 0.01f ? 0.01f : Time.fixedDeltaTime;
        rb.velocity = new Vector2(speed * deltaTime * SpeedMultiplier, rb.velocity.y);
    }


    /**
     * Solo salta si esta en el suelo
     * o si ha caido encima de un enemigo y lo ha matado.
     */
    private void Jump(bool force = false) {
        if (!force && !IsGrounded())
            return;
        manager.PlaySoundEffect(PlayManager.Effects.JUMP);
        rb.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
        jump = false;
    }
    /**
     * Hace un flip de la animacion cuando
     * el player cambia de direccion.
     */
    private void Flip() {
        isFacingRight = !isFacingRight;
        sr.flipX = !sr.flipX;
        manager.PlaySoundEffect(PlayManager.Effects.FLIP);
    }


    /**
     * Animation event llamado 3 veces durante la animacion de correr
     * para hacer sonar los pasos, que son sonidos muy cortos. 
     * De esta forma no suena fuera de lugar si el jugador no esta
     * corriendo.
     * Hay dos sonidos para poder simular el pie izquierdo y el derecho.
     */
    public void AnimationEventStep(int i) {
        if (i == 0)
            manager.PlaySoundEffect(PlayManager.Effects.STEP1);
        else
            manager.PlaySoundEffect(PlayManager.Effects.STEP2);
    }



    /**
     * Metodo llamado desde PlayManager para comunicarle al personaje
     * que es su turno y que se prepare.
     * En caso de no ser un jugador toma el control la IA y hace el 
     * calculo para disparar.
     * Pone a 1 la masa del Rigidbody2D para que el jugador pueda
     * mover facilmente su personaje.
     * Tambien llama al metodo del slot para actualizar su UI.
     */
    public void EnterTurn() {
        projectilesLaunched = projectilesExploded = 0;
        isThisTurn = true;
        canClick = true;
        hasFinished = cancelClick = hasLeftClick = hasClicked = false;
        rb.mass = 1;
        //if (!isPlayer && false)
        if (!isPlayer)
            StartCoroutine(CalculateForce());
        slot.ExecuteTurn();
    }
    /**
     * Metodo contrario, llamado al finalizar el turno para 
     * actualizar la informacion necesaria.
     * Cambia la masa del Rigidbody2D para hacerlo muy pesado
     * y asi evitar que otros jugadores puedan empujarlo (demasiado)
     * al moverse.
     * Tambien cancela el click, en caso de que el jugador estuviese 
     * apuntando sin haber disparado.
     */
    public void ExitTurn() {
        rb.mass = 50;
        isThisTurn = false;
        cancelClick = true;
        animator.SetFloat("SpeedX", 0);
        slot.ExitTurn();
    }

    /**
     * Metodo de comprobacion utilizado por PlayManager para saber 
     * si el personaje ha acabado su turno (incluyendo si ha disparado
     * y el proyectil ha explotado, para evitar que empiece otro turno
     * con un proyectil en el aire).
     */
    public bool HasFinished() {
        if (hasFinished) {
            hasFinished = false;
            return true;
        }
        return false;
    }

    public bool HasShooted() {
        return hasShooted;
    }

    /**
     * Metodo llamado desde Projectile para el caso de la bomba 
     * magnetizada, para comprobar si hay otros personajes cerca.
     */
    public Transform IsOtherCharacterClose(Vector3 projectilePos) {
        Transform[] others = manager.GetOthersPositions(this);
        foreach (Transform other in others) {
            if (Vector2.Distance(projectilePos, other.position) < manager.GreenSpecialDistance)
                return other;
        }
        return null;
    }

    public Sprite GetSpecialWepSprite() {
        return WepsPrefabs[1].GetComponent<SpriteRenderer>().sprite;
    }



    /**
     * Metodo principal encargado de disparar el/los proyectil/es 
     * dependiendo del personaje que los lance.
     * Primero comprueba el arma seleccionada por el personaje y 
     * dependiendo de que tipo de disparo sea llama al metodo TypedShoot
     * (encargado de hacer un disparo basico) o a uno de los dos metodos
     * (Pink o Brown) que tienen una funcionalidad distinta.
     * 
     * En el caso de los personajes Pink y Brown, al ser una 
     * funcionalidad distinta cada uno ha sido necesario hacer dos metodos 
     * distintos, pero en los casos de Blue y Green se hace en la misma 
     * clase Projectile, ya que solo consisten en quitarle la gravedad al 
     * Blue en comprobar constantemente si hay otros personajes (Green).
     */
    private void DoShoot(float angleZ, float force) {
        force *= manager.MaxForce;
        
        if (manager.GetSelectedWeapon() == 0) {
            projectilesLaunched = 1;
            TypedShoot(angleZ, force, ArrowSr.transform.position, Projectile.Type.BASE);
            manager.PlaySoundEffect(PlayManager.Effects.SHOOT);
        } else {
            string n = name.ToLower();
            if (n.Contains("blue")) {
                projectilesLaunched = 1;
                TypedShoot(angleZ, force, ArrowSr.transform.position, Projectile.Type.BLUE);
                manager.PlaySoundEffect(PlayManager.Effects.BLUESHOOT);
            } else if (n.Contains("pink")) {
                projectilesLaunched = 3;
                PinkSpecialShoot();
                manager.PlaySoundEffect(PlayManager.Effects.PINKSHOOT);
            } else if (n.Contains("brown")) {
                projectilesLaunched = manager.BrownNumArrows;
                BrownSpecialShoot(angleZ, force);
                manager.PlaySoundEffect(PlayManager.Effects.BROWNSHOOT);
            } else {
                projectilesLaunched = 1;
                TypedShoot(angleZ, force, ArrowSr.transform.position, Projectile.Type.GREEN);
                manager.PlaySoundEffect(PlayManager.Effects.GREENSHOOT);
            }
        }
        
        if (manager.GetSelectedWeapon() == 1) {
            SpecialWepUnits--;
            if (SpecialWepUnits == 0)
                manager.SetSpecialWepUnits(SpecialWepUnits);
        }
        hasShooted = true;
    }

    /**
     * Metodo basico que ejecuta un disparo dado un angulo de tiro, la fuerza,
     * la posicion inicial y el tipo de proyectil.
     */
    private void TypedShoot(float angleZ, float force, Vector3 pos, Projectile.Type type) {
        GameObject projectile = Instantiate(WepsPrefabs[manager.GetSelectedWeapon()],
                                pos,
                                Quaternion.AngleAxis(angleZ, Vector3.forward));
        projectile.GetComponent<Projectile>()
            .Shoot(this, force, manager.NumOfAngles, manager.CollisionMask, manager.TerrainMask, type);
    }

    /**
     * Metodo que ejecuta el disparo del personaje Brown, el cual consiste
     * unicamente en disparar varios misiles.
     * Para ello utiliza una corutina que lanza un numero determinado de 
     * proyectiles con un angulo aleatorio comprendido en un rango y 
     * esperando un tiempo aleatorio entre proyectil y proyectil de 
     * aproximadamente 0.2 segundos.
     */
    private void BrownSpecialShoot(float angleZ, float force) {
        StartCoroutine(RainOfMissilesEffect(angleZ, force));
    }
    private IEnumerator RainOfMissilesEffect(float angleZ, float force) {
        for (int i = 0; i < manager.BrownNumArrows; i++) {
            float randAngle = Random.Range(-10, 10) + angleZ;
            TypedShoot(randAngle, force, ArrowSr.transform.position, Projectile.Type.BROWN);
            yield return new WaitForSeconds(Random.Range(0.18f, 0.22f));
        }
    }

    /**
     * Metodo que ejecuta el disparo del personaje Pink, el cual consiste
     * en disparar 3 proyectiles desde el cielo para que caigan 
     * aproximadamente encima de los otros 3 personajes del juego.
     * Para ello solo necesita un pequeño rango aleatorio para que 
     * no caigan literalmente encima de los otros personajes y pasarle
     * el angulo de 270 grados (equivalente a apuntar hacia abajo).
     */
    private void PinkSpecialShoot() {
        Vector3[] positions = manager.GetOthersUpOutsidePositions(this);
        for (int i = 0; i < positions.Length; i++) {
            Vector3 pos = positions[i];
            pos.x += Random.Range(-0.6f, 0.6f);
            TypedShoot(270f, 0, pos, Projectile.Type.PINK);
        }
    }



    /**
     * Metodo llamado desde Projectile para alertar al personaje emisor
     * de que el proyectil ha explotado, pasandole por parametros la 
     * cantidad de puntos obtenida.
     * Dado que al explotar el proyectil acaba el turno, para evitar que
     * en los casos especiales de Brown y Pink acabe en mitad de la accion
     * tiene dos contadores: para los misiles lanzados y los explotados.
     * Cuando ambos coinciden entonces significa que todos los lanzados 
     * han explotado, por lo que ya puede finalizar el turno
     */
    public void Exploded(int p) {
        projectilesExploded++;
        manager.PlaySoundEffect(PlayManager.Effects.EXPLOSION);
        hasFinished = projectilesLaunched == projectilesExploded;
        hasShooted = false;
        slot.AddPoints(p);
    }


    /**
     * Metodo llamado desde Projectile, por el proyectil que ha impactado
     * contra este personaje (o cerca de el) para realizar las acciones
     * oportunas y calcular el daño producido que sera equivalente a la 
     * puntuacion que consiga el personaje que ha lanzado este proyectil.
     */
    public int TakeDamage(CircleCollider2D circle, float radius, int dmg) {
        Vector2 circlePos = circle.transform.position;
        Vector2 thisPos = transform.position;
        float pct = Vector2.Distance(circlePos, thisPos) / radius;
        dmg = (int)(dmg * pct);
        rb.AddForce(Vector2.one * JumpForce * pct);
        slot.TakeDamage(dmg);
        if (slot.Life == 0) {
            Die();
        } else {
            animator.SetTrigger("DoHit");
            manager.PlaySoundEffect(PlayManager.Effects.HIT);
        }
        return dmg;
    }

    /**
     * Metodo utilizado cuando el personaje muere, ya sea por quedarse sin vida
     * o por haber caido al vacio, para posicionarlo en un punto seguro y
     * actualizar su informacion (puntos y restaurar la vida).
     */
    private void Die() {
        animator.SetTrigger("DoHit");
        manager.PlaySoundEffect(PlayManager.Effects.DIE);
        transform.position = manager.GetSafeRespawnPoint().position;
        slot.AddPoints(-manager.DiePoints);
        slot.RestoreLife();
    }




    /**
     * Corutina ejecutada cuando el jugador pulsa con el mouse sobre la pantalla.
     * Primero activa la flecha, y mientras el jugador no cancele la accion (ya
     * sea soltando el mouse para disparar o pulsando ESCAPE) la flecha rota
     * apuntando hacia donde el jugador apunta y modifica su escala y color 
     * segun la potencia que el jugador le de (rueda del mouse).
     * Cuando el jugador disparar, si no ha sido cancelado antes, se ejecuta el
     * metodo de disparo visto previamente con el angulo y la fuerza.
     * Por ultimo se resetea la flecha para dejarla al tamaño y color que estaba 
     * y hacerla invisible de nuevo.
     */
    private IEnumerator LoadShootEffect() {
        Camera cam = Camera.main;
        ArrowCenterTr.gameObject.SetActive(true);
        float progress = 0;
        while (click && !cancelClick) {
            Vector2 pos = cam.ScreenToWorldPoint(Input.mousePosition);
            Vector2 arrowPos = ArrowCenterTr.position;
            Vector3 dir = (pos - arrowPos).normalized;
            Vector3 angles = ArrowCenterTr.eulerAngles;
            float angle = Vector3.SignedAngle(ArrowCenterTr.up, dir, Vector3.forward);
            Vector3 desiredAngle = new Vector3(0, 0, ArrowCenterTr.eulerAngles.z + angle);
            ArrowCenterTr.eulerAngles = Vector3.Lerp(ArrowCenterTr.eulerAngles, desiredAngle, 1);
            //
            progress = mouseWheel / MaxWheel;
            manager.SetShootingForceText((int)(progress * 100));
            ArrowSr.color = Color.Lerp(MinColorArrow, MaxColorArrow, progress);
            //
            ArrowTr.localScale = Vector3.Lerp(normalScale, bigScale, progress);
            yield return null;
        }
        mouseWheel = 0;
        ArrowCenterTr.gameObject.SetActive(false);
        if (!cancelClick)
            DoShoot(ArrowCenterTr.eulerAngles.z, progress);
        canClick = cancelClick;
        ArrowCenterTr.eulerAngles = Vector3.zero;
        ArrowTr.localScale = normalScale;
        ArrowSr.color = MinColorArrow;
        yield return null;
    }



    /**
     * Utiliza RaycastHit2D desde dos posiciones en este caso
     * (extremos laterales del player) para comprobar
     * si esta en el suelo.
     */
    private bool IsGrounded() {
        foreach (Transform tr in groundCheckers) {
            RaycastHit2D hit = Physics2D.Raycast(tr.position, Vector2.down, checkDistance, layerMaskGround.value);
            if (hit.transform) {
                return true;
            }
        }
        return false;
    }



    /**
     * Metodo que comprueba el input del jugador cuando es su turno.
     * 
     * Para evitar que el personaje dispare al seleccionar un arma de la UI
     * comprueba el GameObject sobre el que se ha hecho el click, y si tiene
     * un componente CanvasRenderer significa que es un componente de la UI, 
     * por lo que en ese caso no tiene en cuenta el click del mouse.
     * 
     * Para poder hacer que el jugador pueda cancelar un disparo al pulsar 
     * ESCAPE, ha sido necesario introducir dos booleanos que tienen en cuenta
     * cuando el jugador ha pulsado y cuando ha dejado de pulsar.
     * 
     * Por ultimo la rueda del mouse, para asegurar que este dentro del rango
     * (un minimo de 0.25 y maximo de 1) se utilizan las funcoines Max y Min
     * de Mathf. Tiene un minimo de 0.25 para que siempre haya una minima
     * fuerza de disparo, ya que sino al instanciar un proyectil este cae
     * directamente.
     */
    private void CheckInput() {
        speed = Input.GetAxisRaw("Horizontal");
        jump = Input.GetButtonDown("Jump");
        cancelClick = Input.GetButtonDown("Cancel");
        
        GameObject go = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;
        hasClicked = Input.GetMouseButtonDown(0) &&
            (go == null || go.GetComponent<CanvasRenderer>() == null);
        hasLeftClick = Input.GetMouseButtonUp(0) || click && cancelClick;
        mouseWheel = Mathf.Min(MaxWheel, Mathf.Max(0.25f, mouseWheel + Input.GetAxis("Mouse ScrollWheel")));
    }



    /**
     * Metodo que la IA utiliza para hacer el calculo del tirpo parabolico.
     * 
     * Antes de calcularlo escoge arma, con un 15% de probabilidades de que
     * elija el arma especial (si tiene unidades).
     * 
     * Para hacer el calculo utiliza las formulas de tiro parabolico, para
     * ello escoje un personaje aleatoriamente y comprueba el angulo del
     * vector que apunta hacia este personaje con el vector vertical up.
     * Luego, para evitar que el tiro sea demasiado directo, divide el angulo
     * entre 5, para que apunte mas ahcia arriba.
     * Para darle cierta aleatoriedad al tiro se obtiene la altura (diferencia
     * entre Y de ambos personajes), se multiplica por 0.3 (hace el tiro mas 
     * exacto) y se multiplica por un factor aleatorio entre 0.5 y 1.5.
     * 
     * Una vez obtenidas la direccion y la fuerza de tiro, se modifica la 
     * flecha para simular el tiro y se llama al metodo de disparo.
     * 
     * En el caso del proyectil espcial del personaje Blue, dado que es el
     * unico que prescinde de la gravedad, seria un poco absurdo aplicarle
     * el mismo comportamiento parabolico, por lo tanto se tiene en cuenta
     * para modificar su angulo de disparo y ademas se comprueba que haya 
     * espacio directo entre el personaje y su objetivo, ya que tambien seria 
     * un poco absurdo que disparase habiendo terreno de por medio. Si no se
     * cumplen ambos requisitos entonces el personaje Blue disparara el
     * proyectil basico.
     */
    private IEnumerator CalculateForce() {
        if (SpecialWepUnits > 0 && Random.value < 0.15f)
            manager.SelectWeapon(1);
        bool isBlueSpecial = name.ToLower().Contains("blue") && manager.GetSelectedWeapon() == 1;
        yield return new WaitForSeconds(Random.Range(1.5f, 2.5f));

        Vector2 startPos = transform.position;
        Transform ch = manager.GetRandomCharacter(this);
        Vector2 targetPos = ch.position;
        float g = -Physics2D.gravity.y;
        Vector2 direction = targetPos - startPos;
        if (isBlueSpecial) {
            RaycastHit2D hit = Physics2D.Raycast(startPos, direction, 50, manager.CollisionMask);
            if (hit.collider == null || hit.collider.gameObject.layer != LayerMask.NameToLayer("Character")) {
                isBlueSpecial = false;
                manager.SelectWeapon(0);
            }
        }
        float angleWithVertical = Vector2.SignedAngle(Vector2.up, direction);
        float angle = isBlueSpecial ? angleWithVertical : angleWithVertical / 5;
        Debug.DrawRay(startPos, Vector2.up, Color.cyan);
        Debug.DrawRay(startPos, direction, Color.magenta);
        float sin = Mathf.Sin(2 * angle * Mathf.Deg2Rad);
        if (sin == 0)
            sin = 0.01f * Mathf.Sign(angle);
        //direction.y = (targetPos-startPos).y;
        //direction.y *= 0.5f;
        direction.y = 0;
        //float vel = (direction.magnitude + (targetPos-startPos).y * 0.3f) / (sin / g);
        float diff = Random.Range(0.5f, 1.5f);
        float vel = (direction.magnitude + (targetPos - startPos).y * 0.3f * diff) / (sin / g);
        float force = Mathf.Sqrt(Mathf.Abs(vel)) / manager.MaxForce;

        ArrowCenterTr.gameObject.SetActive(true);
        ArrowCenterTr.Rotate(Vector3.forward, angle);
        yield return new WaitForSeconds(0.5f);
        float progress = 0;
        while (progress <= force) {
            progress += Time.deltaTime * 2;
            ArrowTr.localScale = Vector3.Lerp(normalScale, bigScale, progress);
            ArrowSr.color = Color.Lerp(MinColorArrow, MaxColorArrow, progress);
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        DoShoot(angle, force);
        ArrowCenterTr.gameObject.SetActive(false);
        ArrowCenterTr.eulerAngles = Vector3.zero;
        ArrowTr.localScale = normalScale;
        ArrowSr.color = MinColorArrow;
        yield return null;
    }




    /**
     * Para evitar que el jugador se salga de los limites del terreno
     * hay 2 colliders laterales que funcionan como paredes.
     * Ademas, hay un collider que cubre toda la parte inferior del mapa
     * para que el personaje que ha caido muera (llame al metodo Die),
     * siempre que haya puestos seguros sobre los que reaparecer. En caso
     * de no haberlos por completa (o casi completa) destruccion del terreno
     * entonces acaba la partida.
     */
    private void OnCollisionEnter2D(Collision2D collision) {
        int layer = collision.collider.gameObject.layer;
        if (layer == LayerMask.NameToLayer("Limit")) {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1, LayerMask.GetMask("Limit"));
            if (hit.collider == null)
                return;
            Transform tr = manager.GetSafeRespawnPoint();
            if (tr == null)
                manager.GameOver();
            else
                Die();
        }
    }

}
