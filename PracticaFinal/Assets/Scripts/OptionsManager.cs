﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase unica y exclusivamente para modificar el volumen de los efectos y la musica.
 */
public class OptionsManager : MonoBehaviour {

    [SerializeField]
    private Text EffectsNumText, MusicNumText;
    [SerializeField]
    private Slider EffectsSlider, MusicSlider;

    [SerializeField]
    private AudioSource AcceptSound, ToTestEffectSound, ToTestMusicSound;

    [SerializeField]
    private Button PlayEffectsButton, PlayMusicButton, PauseMusicButton;

    private float effects, music;

    private GameManager gm;


    private void Awake() {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        EffectsSlider.value = gm.EffectsVolume;
        MusicSlider.value = gm.MusicVolume;
        OnEffectsChanged();
        OnMusichanged();
    }


    /**
     * Metodos que se ejecutan al mover el boton del slider
     * para modificar el volumen de los efectos o la musica
     */
    public void OnEffectsChanged() {
        effects = EffectsSlider.value;
        EffectsNumText.text = (int)(effects * 100) + "";
        ToTestEffectSound.volume = effects;
    }
    public void OnMusichanged() {
        music = MusicSlider.value;
        MusicNumText.text = (int)(music * 100) + "";
        ToTestMusicSound.volume = music;
    }


    /**
     * Metodos para hacer sonar un efecto o la musica para
     * probar el volumen.
     * Para probar la musica hay una cancion, y a diferencia 
     * de un simple sonido es larga, por lo que al pulsar al 
     * Play se pone el simbolo de Pause para poder pararla.
     */
    public void PlaySound(int i) {
        if (i == 0) {
            ToTestEffectSound.Play();
        } else {
            PlayMusicButton.gameObject.SetActive(false);
            PauseMusicButton.gameObject.SetActive(true);
            ToTestMusicSound.Play();
        }
    }
    public void PauseSound(int i) {
        if (i == 0) {
            ToTestEffectSound.Pause();
        } else {
            PauseMusicButton.gameObject.SetActive(false);
            PlayMusicButton.gameObject.SetActive(true);
            ToTestMusicSound.Pause();
        }
    }


    public void GoBackMenu() {
        AcceptSound.volume = gm.EffectsVolume;
        AcceptSound.Play();
        gm.UpdateVolume(effects, music);
        SceneManager.LoadScene("MenuScene");
    }
}
